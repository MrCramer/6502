

#include "global.h"
#include "vec.h"

const char* const WHITE_SPACE = " \t\n";
const int CODE_OFFSET = 0x600;


/* Assemble Verb
 *
 * Assembly is done with two passes which both leverage the same code. The
 * first pass ignores all jump and branch instruction arguments and notes the
 * position of labels while the second pass ignores the labels and fills in the
 * arguments to the jump and branch instructions.
 */

typedef struct {
  vec_t byte_code;     /* actual byte code */
  vec_t line_mapping;  /* line_mapping[i] is the byte number corresponding to
                          the instruction on or next after line i */
} prog_out_t;

prog_out_t initProgOutStruct() {
  prog_out_t output;
  output.byte_code = newVec(sizeof(unsigned char));
  output.line_mapping = newVec(sizeof(size_t));
  return output;
}

void cleanUpProgOutStruct(prog_out_t *prog) {
  /* leaves object in undefined state with no resources */
  emptyVec(&prog->byte_code);
  emptyVec(&prog->line_mapping);
}

typedef struct {
  char *name;
  int line_num;
} label_node_t;

void cleanUpLabelList(vec_t *vec) {
  for(int i=0; i<vec->size; ++i) {
    label_node_t *lab = getVecElement(vec,i);
    free(lab->name);
  }
  emptyVec(vec);
}

label_node_t* findLabel(char *key, vec_t *label_list) {
  for(int i=0; i<label_list->size; ++i) {
    label_node_t *def = getVecElement(label_list, i);
    if(strcmp(key,def->name)==0) {
      return def;
    }
  }
  return NULL;
}

typedef struct {
  char *name;
  unsigned char value[2];
  bool is_one_byte;
} define_node_t;

void cleanUpDefineList(vec_t *vec) {
  for(int i=0; i<vec->size; ++i) {
    define_node_t *def = getVecElement(vec,i);
    free(def->name);
  }
  emptyVec(vec);
}

define_node_t* findDefine(char *key, vec_t *define_list) {
  for(int i=0; i<define_list->size; ++i) {
    define_node_t *def = getVecElement(define_list, i);
    if(strcmp(key,def->name)==0) {
      return def;
    }
  }
  return NULL;
}

typedef enum {
  ADDRESS_MODE_REGISTER_A,
  ADDRESS_MODE_IMMEDIATE,
  ADDRESS_MODE_ZERO_PAGED,
  ADDRESS_MODE_ZERO_PAGED_X,
  ADDRESS_MODE_ZERO_PAGED_Y,
  ADDRESS_MODE_ABSOLUTE,
  ADDRESS_MODE_ABSOLUTE_X,
  ADDRESS_MODE_ABSOLUTE_Y,
  ADDRESS_MODE_INDEXED_INDIRECT_X,
  ADDRESS_MODE_INDIRECT_INDEXED_Y,
  ADDRESS_MODE_IMPLIED,
  ADDRESS_MODE_JMP_RELATIVE,
  ADDRESS_MODE_JMP_ABSOLUTE,
  ADDRESS_MODE_JMP_INDIRECT,
} address_mode_enum_t;

int parseValue(int line_num, char *token, vec_t *define_list,
               bool *is_one_byte, unsigned char value_bytes[2]) {
  long value;
  char* end;
  size_t token_len = strlen(token);
  /* Check if it's a hex number */
  if(token[0]=='$') {
    if(token_len<2 || token_len>5) {
      printf("%d: hex value must be 1 or 2 bytes\n",line_num);
      return -1;
    }
    value = strtol(token+1, &end, 16);
    if(end-(token+1) != token_len-1) {
      printf("%d: error reading hex value\n",line_num);
      return -1;
    }
    *is_one_byte = (token_len<=3);
    value_bytes[0] = (unsigned char)(value&0xff);
    if(!*is_one_byte) {
      value_bytes[1] = (unsigned char)((value&0xff00)>>8);
    }
    return 0;
  }
  /* Check if it's a decimal number */
  value = strtol(token, &end, 10);
  if(end-token==token_len) {
    if(value<0 || value>0xff) {
      printf("%d: decimal value out of range (must be in [0,255])\n",line_num);
      return -1;
    }
    value_bytes[0] = (unsigned char)value;
    *is_one_byte = true;
    return 0;
  }
  /* Check if it's a define */
  define_node_t *def = findDefine(token, define_list);
  if(!def) {
    printf("%d: unknown identifier %s\n",line_num,token);
    return -1;
  }
  *is_one_byte = def->is_one_byte;
  value_bytes[0] = def->value[0];
  value_bytes[1] = def->value[1];
  return 0;
}

int parseValueArgument(int line_num, char *token, vec_t *define_list,
                       address_mode_enum_t *address_mode, unsigned char value[2]) {
  /* Parses an argument to an instruction that takes a value (not a jump).
   * 
   * Return 0 on success, addressing type is returned in address_mode, the
   * value found is returned in value, if zero based or immediate the value will
   * occupy only value[0].
   */
  bool is_one_byte;
  if(!token) {
    printf("%d: error parsing value, no value found\n",line_num);
    return -1;
  }
  size_t token_len = strlen(token);

  if(token[0]=='A' && token_len==1) {
    *address_mode = ADDRESS_MODE_REGISTER_A;
    return 0;
  }

  if(token[0]=='#') {
    /* immediate referencing, assume rest of token is the value */
    *address_mode = ADDRESS_MODE_IMMEDIATE;
    if(parseValue(line_num,token+1,define_list,&is_one_byte,value)) return -1;
    if(!is_one_byte) {
      printf("%d: immediate referencing cannot handle multi-byte values",
             line_num);
      return -1;
    }
    return 0;
  }
  if(token[0]=='(') {
    if(token_len<4) {
      printf("%d: syntax error in reference, trying to read an indirect "
             "reference with indexing\n",line_num);
      return -1;
    }
    if(token[token_len-3] == ',' &&
       (token[token_len-2]=='X'||token[token_len-2]=='x') && 
       token[token_len-1]==')') {
      *address_mode = ADDRESS_MODE_INDEXED_INDIRECT_X;
    } else if(token[token_len-3] == ')' && token[token_len-2]==','&& 
              (token[token_len-1]=='Y' || token[token_len-1]=='y')) {
      *address_mode = ADDRESS_MODE_INDIRECT_INDEXED_Y;
    } else {
      printf("%d: syntax error in reference, trying to read an indirect "
             "reference with indexing\n",line_num);
      return -1;
    }
    token[token_len-3] = '\0'; /* cut off value for parsing */
    if(parseValue(line_num,token+1,define_list,&is_one_byte,value)) return -1;
    if(!is_one_byte) {
      printf("%d: value too large for reference type\n",line_num);
      return -1;
    }
    return 0;
  }

  if(token_len > 2 && token[token_len-2]==',') {
    /* Zero page or absolute, indexed */
    char index_char = token[token_len-1];
    token[token_len-2] = '\0'; /* Cut off where the value should be */
    if(parseValue(line_num,token,define_list,&is_one_byte,value)) return -1;

    if(is_one_byte) {
      if(index_char=='X' || index_char=='x') {
        *address_mode = ADDRESS_MODE_ZERO_PAGED_X;
        return 0;
      } else if(index_char=='Y' || index_char=='y') {
        *address_mode = ADDRESS_MODE_ZERO_PAGED_Y;
        return 0;
      } else {
        printf("%d: only X and Y registers are allowed for absolute indexed "
               "referencing\n", line_num);
        return -1;
      }
    } else {
      if(index_char=='X' || index_char=='x') {
        *address_mode = ADDRESS_MODE_ABSOLUTE_X;
        return 0;
      } else if(index_char=='Y' || index_char=='y') {
        *address_mode = ADDRESS_MODE_ABSOLUTE_Y;
        return 0;
      } else {
        printf("%d: only X and Y registers are allowed for absolute indexed "
               "referencing\n", line_num);
        return -1;
      }
    }
  }

  /* only types left are absolute or zero paged */
  if(parseValue(line_num,token,define_list,&is_one_byte,value)) return -1;
  if(is_one_byte) {
    *address_mode = ADDRESS_MODE_ZERO_PAGED;
  } else {
    *address_mode = ADDRESS_MODE_ABSOLUTE;
  }
  return 0;
}

int parseBranchArgument(int pass, int line_num, char *token,
                        vec_t *label_list, prog_out_t *program,
                        unsigned char value_bytes[2]) {
  long value;
  label_node_t *label;
  size_t my_pos,jmp_pos;
  int diff;

  if(pass==1) {
    /* don't even bother looking up on first pass, just say it worked */
    return 0;
  }

  if(token[0]=='$') {
    if(strlen(token)!=3) {
      printf("%d: invalid branch argument (must be label or 2 digit hex)\n",
             line_num);
      return -1;
    }
    value = strtol(token+1,NULL,16);
    value_bytes[0] = (unsigned char)(value & 0xFF);
    return 0;
  }
  
  label = findLabel(token,label_list);
  if(!label) {
    printf("%d: unable to find label %s\n", line_num, token);
    return -1;
  }
  
  if(       line_num >= program->line_mapping.size || 
     label->line_num >= program->line_mapping.size) {
    printf("%d: internal error, %s(%d): line_mapping too small\n",
           line_num,__FILE__,__LINE__);
    return -1;
  }
  
  /* position of program counter is +2 from the instruction's. */
  my_pos  = *(size_t*)(getVecElement(&program->line_mapping,line_num))+2;
  jmp_pos = *(size_t*)(getVecElement(&program->line_mapping,label->line_num));
  diff = (int)jmp_pos-(int)my_pos;
  if(diff < -128 || diff > 127) {
    printf("%3d: branch distance out of range, %d not in [-128,127]\n",
           line_num,diff);
    printf("     label: %s\n",token);
    return -1;
  }
  value_bytes[0] = (unsigned char)(signed char)diff;
  return 0;
}

int parseJumpArgument(int prog_offset, int pass, int line_num, char *token,
                      vec_t *label_list, vec_t *define_list, prog_out_t *program,
                      address_mode_enum_t *address_mode,
                      unsigned char value_bytes[2]) {
  int jmp_pos;
  size_t token_length = strlen(token);
  label_node_t *label;

  if(pass==1) {
    /* Both possible are 2 bytes wide, all instructions can take absolute */
    *address_mode = ADDRESS_MODE_JMP_ABSOLUTE;
    return 0;
  }

  if(token[0]=='$') {
    long value = strtol(token+1,NULL,16);
    if(token_length!=5) {
      printf("%d: instruction must have absolute or indirect addressing\n",
             line_num);
      return -1;
    }
    value_bytes[0] = (unsigned char)(value);
    value_bytes[1] = (unsigned char)(value >> 8);

    *address_mode = ADDRESS_MODE_JMP_ABSOLUTE;
    return 0;
  }

  if(token[0]=='(') {
    bool is_one_byte;
    if(token[token_length-1]!=')') {
      printf("%d: syntax error in jump argument\n",line_num);
      return -1;
    }
    token[token_length-1] = '\0';
    if(parseValue(line_num,token+1,define_list,&is_one_byte,value_bytes)) {
      return -1;
    }
    if(is_one_byte) {
      printf("%d: instruction must have aboslute or indirect addressing\n",
             line_num);
      return -1;
    }
    *address_mode = ADDRESS_MODE_JMP_INDIRECT;
    return 0;
  }

  *address_mode = ADDRESS_MODE_JMP_ABSOLUTE;
  label = findLabel(token,label_list);
  if(!label) {
    printf("%d: unable to find label %s\n", line_num, token);
    return -1;
  }
  
  if(       line_num >= program->line_mapping.size || 
     label->line_num >= program->line_mapping.size) {
    printf("%d: internal error, %s(%d): line_mapping too small\n",
           line_num,__FILE__,__LINE__);
    return -1;
  }
  jmp_pos = (int)*(size_t*)(getVecElement(&program->line_mapping,label->line_num))
            + prog_offset;
  value_bytes[0] = (unsigned char)(jmp_pos);
  value_bytes[1] = (unsigned char)(jmp_pos >> 8);
  return 0;
}

int parseInstruction(int prog_offset, int pass, int line_num, char *token,
                     vec_t *label_list, vec_t *define_list,
                     prog_out_t *program) {
  address_mode_enum_t address_mode;
  unsigned char value[2],op_code;
  char *ref_token = strtok(NULL, WHITE_SPACE);
  /* treat a comment as if there were no arg at all */
  if(ref_token && ref_token[0]==';') ref_token = NULL;
  
  if(strcasecmp(token, "ADC")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0x69; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x65; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x75; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x6D; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x7D; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0x79; break;
      case ADDRESS_MODE_INDEXED_INDIRECT_X: op_code = 0x61; break;
      case ADDRESS_MODE_INDIRECT_INDEXED_Y: op_code = 0x71; break;
      default:
        printf("%d: Bad reference type for ADC\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "SBC")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0xE9; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xE5; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0xF5; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xED; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0xFD; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0xF9; break;
      case ADDRESS_MODE_INDEXED_INDIRECT_X: op_code = 0xE1; break;
      case ADDRESS_MODE_INDIRECT_INDEXED_Y: op_code = 0xF1; break;
      default:
        printf("%d: Bad reference type for SBC\n",line_num);
        return -1;
    }
   } else
  if(strcasecmp(token, "AND")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0x29; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x25; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x35; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x2D; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x3D; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0x39; break;
      case ADDRESS_MODE_INDEXED_INDIRECT_X: op_code = 0x21; break;
      case ADDRESS_MODE_INDIRECT_INDEXED_Y: op_code = 0x31; break;
      default:
        printf("%d: Bad reference type for AND\n",line_num);
        return -1;
    }
   } else
  if(strcasecmp(token, "ORA")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0x09; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x05; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x15; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x0D; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x1D; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0x19; break;
      case ADDRESS_MODE_INDEXED_INDIRECT_X: op_code = 0x01; break;
      case ADDRESS_MODE_INDIRECT_INDEXED_Y: op_code = 0x11; break;
      default:
        printf("%d: Bad reference type for ORA\n",line_num);
        return -1;
    }
   } else
  if(strcasecmp(token, "ASL")==0) {
    if(!ref_token) {
      address_mode = ADDRESS_MODE_REGISTER_A;
    } else if(parseValueArgument(line_num,ref_token,define_list,
              &address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_REGISTER_A:         op_code = 0x0A; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x06; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x16; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x0E; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x1E; break;
      default:
        printf("%d: Bad reference type for ASL\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "INC")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xE6; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0xF6; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xEE; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0xFE; break;
      default:
        printf("%d: Bad reference type for INC\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "LSR")==0) {
    if(!ref_token) {
      address_mode = ADDRESS_MODE_REGISTER_A;
    } else if(parseValueArgument(line_num,ref_token,define_list,
              &address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_REGISTER_A:         op_code = 0x4A; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x46; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x56; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x4E; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x5E; break;
      default:
        printf("%d: Bad reference type for LSR\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "ROL")==0) {
    if(!ref_token) {
      address_mode = ADDRESS_MODE_REGISTER_A;
    } else if(parseValueArgument(line_num,ref_token,define_list,
              &address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_REGISTER_A:         op_code = 0x2A; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x26; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x36; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x2E; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x3E; break;
      default:
        printf("%d: Bad reference type for ROL\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "ROR")==0) {
    if(!ref_token) {
      address_mode = ADDRESS_MODE_REGISTER_A;
    } else if(parseValueArgument(line_num,ref_token,define_list,
              &address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_REGISTER_A:         op_code = 0x6A; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x66; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x76; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x6E; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x7E; break;
      default:
        printf("%d: Bad reference type for ROR\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "LDA")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0xA9; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xA5; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0xB5; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xAD; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0xBD; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0xB9; break;
      case ADDRESS_MODE_INDEXED_INDIRECT_X: op_code = 0xA1; break;
      case ADDRESS_MODE_INDIRECT_INDEXED_Y: op_code = 0xB1; break;
      default:
        printf("%d: Bad reference type for LDA\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "LDX")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0xA2; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xA6; break;
      case ADDRESS_MODE_ZERO_PAGED_Y:       op_code = 0xB6; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xAE; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0xBE; break;
      default:
        printf("%d: Bad reference type for LDX\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "LDY")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0xA0; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xA4; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0xB4; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xAC; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0xBC; break;
      default:
        printf("%d: Bad reference type for LDY\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "STA")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x85; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x95; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x8D; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x9D; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0x99; break;
      case ADDRESS_MODE_INDEXED_INDIRECT_X: op_code = 0x81; break;
      case ADDRESS_MODE_INDIRECT_INDEXED_Y: op_code = 0x91; break;
      default:
        printf("%d: Bad reference type for STA\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "STX")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x86; break;
      case ADDRESS_MODE_ZERO_PAGED_Y:       op_code = 0x96; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x8E; break;
      default:
        printf("%d: Bad reference type for STX\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "STY")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x84; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x94; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x8C; break;
      default:
        printf("%d: Bad reference type for STY\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "TAX")==0) {
    op_code = 0xAA;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "TXA")==0) {
    op_code = 0x8A;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "DEX")==0) {
    op_code = 0xCA;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "INX")==0) {
    op_code = 0xE8;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "TAY")==0) {
    op_code = 0xA8;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "TYA")==0) {
    op_code = 0x98;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "DEY")==0) {
    op_code = 0x88;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "INY")==0) {
    op_code = 0x88;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "BPL")==0) {
    if(parseBranchArgument(pass,line_num,ref_token,label_list,program,value)) {
      return -1;
    }
    op_code = 0x10;
    address_mode = ADDRESS_MODE_JMP_RELATIVE;
  } else
  if(strcasecmp(token, "BMI")==0) {
    if(parseBranchArgument(pass,line_num,ref_token,label_list,program,value)) {
      return -1;
    }
    op_code = 0x30;
    address_mode = ADDRESS_MODE_JMP_RELATIVE;
  } else
  if(strcasecmp(token, "BVC")==0) {
    if(parseBranchArgument(pass,line_num,ref_token,label_list,program,value)) {
      return -1;
    }
    op_code = 0x50;
    address_mode = ADDRESS_MODE_JMP_RELATIVE;
  } else
  if(strcasecmp(token, "BVS")==0) {
    if(parseBranchArgument(pass,line_num,ref_token,label_list,program,value)) {
      return -1;
    }
    op_code = 0x70;
    address_mode = ADDRESS_MODE_JMP_RELATIVE;
  } else
  if(strcasecmp(token, "BCC")==0) {
    if(parseBranchArgument(pass,line_num,ref_token,label_list,program,value)) {
      return -1;
    }
    op_code = 0x90;
    address_mode = ADDRESS_MODE_JMP_RELATIVE;
  } else
  if(strcasecmp(token, "BCS")==0) {
    if(parseBranchArgument(pass,line_num,ref_token,label_list,program,value)) {
      return -1;
    }
    op_code = 0xB0;
    address_mode = ADDRESS_MODE_JMP_RELATIVE;
  } else
  if(strcasecmp(token, "BNE")==0) {
    if(parseBranchArgument(pass,line_num,ref_token,label_list,program,value)) {
      return -1;
    }
    op_code = 0xD0;
    address_mode = ADDRESS_MODE_JMP_RELATIVE;
  } else
  if(strcasecmp(token, "BEQ")==0) {
    if(parseBranchArgument(pass,line_num,ref_token,label_list,program,value)) {
      return -1;
    }
    op_code = 0xF0;
    address_mode = ADDRESS_MODE_JMP_RELATIVE;
  } else
  if(strcasecmp(token, "BRK")==0) {
    op_code = 0x00;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "CMP")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0xC9; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xC5; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0xD5; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xCD; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0xDD; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0xD9; break;
      case ADDRESS_MODE_INDEXED_INDIRECT_X: op_code = 0xC1; break;
      case ADDRESS_MODE_INDIRECT_INDEXED_Y: op_code = 0xD1; break;
      default:
        printf("%d: Bad reference type for CMP\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "CPX")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0xE0; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xE4; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xEC; break;
      default:
        printf("%d: Bad reference type for CPX\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "CPY")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0xC0; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xC4; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xCC; break;
      default:
        printf("%d: Bad reference type for CPY\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "DEC")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0xC6; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0xD6; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0xCE; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0xDE; break;
      default:
        printf("%d: Bad reference type for DEC\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "EOR")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_IMMEDIATE:          op_code = 0x49; break;
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x45; break;
      case ADDRESS_MODE_ZERO_PAGED_X:       op_code = 0x55; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x4D; break;
      case ADDRESS_MODE_ABSOLUTE_X:         op_code = 0x5D; break;
      case ADDRESS_MODE_ABSOLUTE_Y:         op_code = 0x59; break;
      case ADDRESS_MODE_INDEXED_INDIRECT_X: op_code = 0x41; break;
      case ADDRESS_MODE_INDIRECT_INDEXED_Y: op_code = 0x51; break;
      default:
        printf("%d: Bad reference type for EOR\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "TXS")==0) {
    op_code = 0x9A;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "TSX")==0) {
    op_code = 0xBA;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "PHA")==0) {
    op_code = 0x48;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "PLA")==0) {
    op_code = 0x68;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "PHP")==0) {
    op_code = 0x08;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "PLP")==0) {
    op_code = 0x28;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "CLC")==0) {
    op_code = 0x18;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "SEC")==0) {
    op_code = 0x38;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "CLI")==0) {
    op_code = 0x58;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "SEI")==0) {
    op_code = 0x78;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "CLV")==0) {
    op_code = 0xB8;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "CLD")==0) {
    op_code = 0xD8;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "SED")==0) {
    op_code = 0xF8;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "JMP")==0) {
    if(parseJumpArgument(prog_offset,pass,line_num,ref_token,
                         label_list,define_list,program,
                         &address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_JMP_ABSOLUTE: op_code = 0x4C; break;
      case ADDRESS_MODE_JMP_INDIRECT: op_code = 0x6C; break;
      default:
        printf("%d: Bad reference type for JMP\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "JSR")==0) {
    if(parseJumpArgument(prog_offset,pass,line_num,ref_token,
                         label_list,define_list,program,
                         &address_mode,value)) {
      return -1;
    }
    if(address_mode!=ADDRESS_MODE_JMP_ABSOLUTE) {
      printf("%d: Bad reference type for JSR\n",line_num);
      return -1;
    }
    op_code = 0x20; 
  } else
  if(strcasecmp(token, "RTI")==0) {
    op_code = 0x40;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "RTS")==0) {
    op_code = 0x60;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else
  if(strcasecmp(token, "BIT")==0) {
    if(parseValueArgument(line_num,ref_token,define_list,&address_mode,value)) {
      return -1;
    }
    switch(address_mode) {
      case ADDRESS_MODE_ZERO_PAGED:         op_code = 0x24; break;
      case ADDRESS_MODE_ABSOLUTE:           op_code = 0x2C; break;
      default:
        printf("%d: Bad reference type for BIT\n",line_num);
        return -1;
    }
  } else
  if(strcasecmp(token, "NOP")==0) {
    op_code = 0xEA;
    address_mode = ADDRESS_MODE_IMPLIED;
  } else {
    printf("%d: Unknown instruction (%s)\n", line_num, token);
    return -1;
  }
  /* Push the op_code */
  if(pushVec(&op_code,&program->byte_code)) return -1;
  /* Push the references */
  switch(address_mode) {
    case ADDRESS_MODE_IMPLIED:
    case ADDRESS_MODE_REGISTER_A:
      break;
    case ADDRESS_MODE_IMMEDIATE:
    case ADDRESS_MODE_ZERO_PAGED:
    case ADDRESS_MODE_ZERO_PAGED_X:
    case ADDRESS_MODE_ZERO_PAGED_Y:
    case ADDRESS_MODE_INDEXED_INDIRECT_X:
    case ADDRESS_MODE_INDIRECT_INDEXED_Y:
    case ADDRESS_MODE_JMP_RELATIVE:
      if(pushVec(&value[0],&program->byte_code)) return -1;
      break;
    case ADDRESS_MODE_ABSOLUTE:
    case ADDRESS_MODE_ABSOLUTE_X:
    case ADDRESS_MODE_ABSOLUTE_Y:
    case ADDRESS_MODE_JMP_ABSOLUTE:
    case ADDRESS_MODE_JMP_INDIRECT:
      if(pushVec(&value[0],&program->byte_code)) return -1;
      if(pushVec(&value[1],&program->byte_code)) return -1;
      break;
  }
  return 0;
}

int parseDefine(int line_num, vec_t *define_list) {
  /* Parses a define line adding the define into define_list.
   *
   * Assumes that the file's line is being processed by strtok and that the last
   * token was the keyword 'define'.
   *
   * returns 0 on success.
   */
  define_node_t define;
  char *token;

  /* Read the identifier */
  token = strtok(NULL, WHITE_SPACE);
  if(!token || token[0]==';') {
    printf("%d: no identifier after define\n",line_num);
    return -1;
  }
  if(findDefine(token,define_list)) {
    printf("%d: redefinition of %s\n",line_num,token);
    return -1;
  }
  /* Copy the id */
  define.name = malloc(strlen(token)+1);
  if(!define.name) {
    FAILED_ALLOC;
    return -1;
  }
  strcpy(define.name,token);
  token = strtok(NULL, WHITE_SPACE);
  if(!token || token[0]==';') {
    free(define.name);
    printf("%d: no value after define identifier\n",line_num);
    return -1;
  }
  
  /* Read the value */
  if(parseValue(line_num,token,define_list,&define.is_one_byte,define.value)) {
    return -1;
  }
  
  /* Push onto vector */
  if(pushVec(&define,define_list)) {
    free(define.name);
    return -1;
  }
  return 0;
}

int parseCodeLine(int pass, int line_num, char *line,
                  vec_t *label_list, vec_t *define_list, prog_out_t *program) {
  /* If a define is found it is added to define_list. On success returns 0.
   *
   * Modifies line but does not free.
   */
  size_t token_length;
  char *token;

  if(pass==1) {
    /* add the current byte_code position to the line mapping */
    if(pushVec(&program->byte_code.size,&program->line_mapping)) return -1;
  }

  token = strtok(line, WHITE_SPACE);
  if(!token || token[0]==';') {
    /* empty line, do nothing */
    return 0;
  }

  /* consume any labels */
  token_length = strlen(token);
  while(token[token_length-1]==':') {
    if(pass == 1) {
      /* if this is the first pass then register the label */
      label_node_t lab;
      lab.name = malloc(token_length);
      if(!lab.name) {
        FAILED_ALLOC;
        return -1;
      }
      strncpy(lab.name,token,token_length);
      lab.name[token_length-1] = '\0';
      lab.line_num = line_num;
      if(pushVec(&lab,label_list)) {
        free(lab.name);
        return -1;
      }
    }
    token = strtok(NULL,WHITE_SPACE);
    if(!token || token[0]==';') {
      /* rest of line is empty or comment */
      return 0;
    }
    token_length = strlen(token);
  }

  /* Parse define */
  if(token_length==sizeof("define")-1 && strcmp(token, "define")==0) {
    if(pass == 1) {
      if(parseDefine(line_num,define_list)) return -1;
    }
    return 0;
  }

  if(token_length != 3) {
    printf("%d: Syntax error, no label, define, or instruction\n", line_num);
    return -1;
  }

  /* Must be an instruction */
  if(parseInstruction(CODE_OFFSET,pass,line_num,token,
                      label_list,define_list,program)) {
    return -1;
  }
  return 0;
}

int verbAssemble(const char *argv[]) {
  int line_num;
  FILE *code_file, *output_file;
  char *line;
  const char *code_file_name, *output_file_name;
  prog_out_t program;
  vec_t label_list,define_list;
  size_t num_chars;

  code_file_name   = argv[2];
  output_file_name = argv[3];

  /* Open files */
  code_file = fopen(code_file_name, "r");
  if(!code_file) {
    int err = errno;
    const char *error_msg = strerror(err);
    printf("Error opening file %s\n", code_file_name);
    printf("%s\n", error_msg);
    return err;
  }

  output_file = fopen(output_file_name, "wb");
  if(!output_file) {
    int err = errno;
    const char *error_msg = strerror(err);
    printf("Error opening file %s\n", code_file_name);
    printf("%s\n", error_msg);
    fclose(code_file);
    return err;
  }

  /* parse file */

  program = initProgOutStruct();
  define_list = newVec(sizeof(define_node_t));
  label_list  = newVec(sizeof(label_node_t));

  for(int pass=1; pass<3; ++pass) {
    emptyVec(&program.byte_code);
    line_num = 0;
    num_chars = 0;
    line = NULL;
    while(getline(&line,&num_chars,code_file) != -1) {
      if(parseCodeLine(pass,line_num,line,&label_list,&define_list,&program)) {
        cleanUpProgOutStruct(&program);
        cleanUpLabelList(&label_list);
        cleanUpDefineList(&define_list);
        fclose(code_file);
        fclose(output_file);
        free(line);
        return -1;
      }
      line_num ++;
    }
    free(line);
    rewind(code_file);
  }

  num_chars = fwrite(program.byte_code.data,1,
                     program.byte_code.size,output_file);
  if(num_chars!=program.byte_code.size) {
    printf("Error writing to output file\n");
  }

  cleanUpProgOutStruct(&program);
  cleanUpLabelList(&label_list);
  cleanUpDefineList(&define_list);
  fclose(code_file);
  fclose(output_file);
  return 0;
}

