
COMPILER=c99
LINKER=c99

CFLAGS = -Wall -c -I${HOME}/local/include -I${HOME}/local/include/ncurses -g
LFLAGS = -L${HOME}/local/lib -L${HOME}/local/include/ncurses -L${HOME}/local/include -lncurses

SOURCES:=$(wildcard *.c)
OBJECTS=$(SOURCES:%.c=.%.o)

# Pull in auto-dependency info #
-include $(OBJECTS:.%.o=.%.d)


all: interp

test: interp
	./interp assemble snake.txt out
	xxd -r out out.hex
	diff snake_correct.hex out.hex

# Link #
interp: $(OBJECTS)
	$(LINKER) $(LFLAGS) $(OBJECTS) -o interp

# Compile #
.%.o: %.c
	$(COMPILER) $(CFLAGS) $*.c -o .$*.o
	$(COMPILER) -MM -MT .$*.o $(CFLAGS) $*.c > .$*.d

# Clean #
clean:
	rm .*.o .*.d prog
