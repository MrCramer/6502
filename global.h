#ifndef GLOBAL_H
#define GLOBAL_H

#define _GNU_SOURCE

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define FAILED_ALLOC printf("Failed allocation at %s:%d\n", __FILE__, __LINE__);

/* In assemble.c */
int verbAssemble(const char *argv[]);
extern const int CODE_OFFSET;

/* In run.c */
int verbRun(const char *argv[]);

#endif
