#ifndef VEC_H
#define VEC_H

#include "global.h"

/* Vector object - stored data in a linear array
 * 
 * Vector must be emptied via emptyVec.
 *
 * Supports pushing and popping.
 */

typedef struct {
  size_t allocated,size,data_size;
  void *data;
} vec_t;

static inline vec_t newVec(size_t data_size) {
  vec_t vec;
  vec.data_size = data_size;
  vec.allocated = 0;
  vec.size      = 0;
  vec.data      = NULL;
  return vec;
}

static inline void* getVecElement(vec_t *vec, size_t i) {
  return vec->data + i*vec->data_size;
}

static inline void* getVecElementLast(vec_t *vec) {
  return vec->data + (vec->size-1)*vec->data_size;
}

static inline int pushVec(void *object, vec_t *vec) {
  /* Attempts to push object into the end of vec.
   *
   * On success returns 0.
   *
   * On fail vec is unchanged.
   */
  if(vec->allocated < vec->size + 1) {
    /* Double the number of objects and a bit */
    void *new_ptr = realloc(vec->data,
                            (2*vec->allocated+1)*vec->data_size);
    if(!new_ptr) {
      FAILED_ALLOC;
      return -1;
    }
    vec->data = new_ptr;
    vec->allocated = vec->allocated*2+1;
  }
  vec->size += 1;
  memcpy(getVecElementLast(vec), object, vec->data_size);
  return 0;
}

static inline void popVec(vec_t *vec) {
  if(vec->size>0) {
    vec->size -= 1;
  }
}

static inline void emptyVec(vec_t *vec) {
  /* Leaves vec in an empty state with no resources */
  free(vec->data);
  /* After freeing memory, put vec in a clean state */
  *vec = newVec(vec->data_size);
}


#endif

