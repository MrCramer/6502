
#include "global.h"

void printUsage(const char *name) {
  printf("Usage:\n");
  printf(" %s help\n", name);
  printf(" %s assemble code_file out_file\n", name);
  printf(" %s run code_file\n", name);
}

int main(int argc, const char *argv[]) {
  if(argc < 2 || strcmp(argv[1], "help")==0) {
    printUsage(argv[0]);
    return 0;
  }

  if(strcmp(argv[1], "assemble")==0) {
    if(argc < 4) {
      printf("Not enough inputs\n");
      printUsage(argv[0]);
      return 0;
    }
    return verbAssemble(argv);
  }

  if(strcmp(argv[1], "run")==0) {
    if(argc < 3) {
      printf("Not enough inputs\n");
      printUsage(argv[0]);
      return 0;
    }
    return verbRun(argv);
  }

  printf("Uknown verb %s\n", argv[1]);
  printUsage(argv[0]);
  return 0;
}

