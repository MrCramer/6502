
#include "global.h"
#include "vec.h"

/* Program State */

#define MEMORY_SIZE 0x10000

#define BIT_MASK(b) (1<<b)

FILE *logfile;


typedef struct {
  uint8_t  *memory;
  uint16_t program_counter;

  struct {
    bool carry,            zero,
         block_interrupts, decimal_mode,
         brk,              padding,
         overflow,         negative;
  } flag;
  struct {
    /* Accumulator, X, Y, and Stack pointer */
    uint8_t A,X,Y,S;
  } reg;
} program_state_t;

int initProgramState(program_state_t *state) {
  state->memory = malloc(MEMORY_SIZE*sizeof(uint8_t));
  if(!state->memory) {
    return -1;
  }
  state->program_counter = CODE_OFFSET;

  state->flag.carry            = false;
  state->flag.zero             = false;
  state->flag.block_interrupts = false;
  state->flag.decimal_mode     = false;
  state->flag.brk              = false;
  state->flag.overflow         = false;
  state->flag.negative         = false;

  state->reg.A = 0;
  state->reg.X = 0;
  state->reg.Y = 0;
  state->reg.S = 0xff;
  return 0;
}

void cleanUpProgramState(program_state_t *state) {
  free(state->memory);
  state->memory = NULL;
}

int loadProgram(const char *file_name,program_state_t *state) {
  FILE *file;
  unsigned char *ptr;
  file = fopen(file_name, "rb");
  if(!file) {
    int err = errno;
    const char *error_msg = strerror(err);
    printf("Error opening file %s\n", file_name);
    printf(" %s\n", error_msg);
    return err;
  }

  ptr = state->memory+CODE_OFFSET;
  /* read up to the end of the memory bank */
  fread(ptr,1,MEMORY_SIZE-CODE_OFFSET,file);
  if(ferror(file)) {
    int err = errno;
    printf("Error occured reading file %s\n",file_name);
    printf(" %s\n", strerror(err));
    return err;
  }
  if(!feof(file)) {
    printf("File %s is too large to fit in 6502 memory\n",file_name);
    return -1;
  }
  return 0;
}

/* Operation */

uint8_t low(uint16_t addr) {
  return (uint8_t)(addr);
}

uint8_t high(uint16_t addr) {
  return (uint8_t)(addr >> 8);
}

uint16_t combine(uint8_t low, uint8_t high) {
  return (uint16_t)low + ((uint16_t)high << 8);
}

uint8_t readByteFromAddr(program_state_t *state, uint16_t addr) {
  /* Every read should go through here so that we can handle hardware mapped
   * memory such as a random number generator */
  if(addr == 0xFE) {
    return (uint8_t)rand();
  }
  return state->memory[addr];
}

uint16_t readAddrFromAddr(program_state_t *state, uint16_t addr) {
  /* The weird ternary operator is to account for the fact that the addition +1
   * happens to the low byte without consideration for the carry (known bug?) */
  uint8_t temp = readByteFromAddr(state,addr);
  uint16_t one = ((addr&0xFF)==0XFF) ? -0xFF : 1;
  return combine(temp,readByteFromAddr(state,addr+one));
}

uint8_t readPCByte(program_state_t *state) {
  /* Read the value currently pointed to by the program counter and advance
   * the counter.
   *
   * If successful returns 0. Errors on program counter running off the end of
   * memory.
   */
  return readByteFromAddr(state,state->program_counter++);
}

uint8_t getStatusByte(program_state_t *state) {
  return (state->flag.carry            ? BIT_MASK(0) : 0) |
         (state->flag.zero             ? BIT_MASK(1) : 0) |
         (state->flag.block_interrupts ? BIT_MASK(2) : 0) |
         (state->flag.decimal_mode     ? BIT_MASK(3) : 0) |
         (state->flag.brk              ? BIT_MASK(4) : 0) |
         (                               BIT_MASK(5)    ) |
         (state->flag.overflow         ? BIT_MASK(6) : 0) |
         (state->flag.negative         ? BIT_MASK(7) : 0);
}

void setStatusByte(program_state_t *state, uint8_t byte) {
  state->flag.carry            = (bool)(byte & BIT_MASK(0));
  state->flag.zero             = (bool)(byte & BIT_MASK(1));
  state->flag.block_interrupts = (bool)(byte & BIT_MASK(2));
  state->flag.decimal_mode     = (bool)(byte & BIT_MASK(3));
  state->flag.brk              = (bool)(byte & BIT_MASK(4));
  state->flag.padding          = true;
  state->flag.overflow         = (bool)(byte & BIT_MASK(6));
  state->flag.negative         = (bool)(byte & BIT_MASK(7));
}

uint16_t readPCAddr(program_state_t *state) {
  /* Read the two byte value currently pointed to by the program counter and 
   * advance the counter.
   *
   * If successful returns 0. Errors on program counter running off the end of
   * memory.
   */
  uint8_t temp = readByteFromAddr(state,state->program_counter++);
  return combine(temp,readByteFromAddr(state,state->program_counter++));
}

uint16_t getZeroPageRef(program_state_t *state) {
  return readPCByte(state);
}

uint16_t getZeroPageXRef(program_state_t *state) {
  return readPCByte(state) + state->reg.X;
}

uint16_t getZeroPageYRef(program_state_t *state) {
  return readPCByte(state) + state->reg.Y;
}

uint16_t getAbsoluteRef(program_state_t *state) {
  return readPCAddr(state);
}

uint16_t getAbsXRef(program_state_t *state) {
  return readPCAddr(state) + state->reg.X;
}

uint16_t getAbsYRef(program_state_t *state) {
  return readPCAddr(state) + state->reg.Y;
}

uint16_t getIndirectXRef(program_state_t *state) {
  return readAddrFromAddr(state,readPCByte(state)+state->reg.X);
}

uint16_t getIndirectYRef(program_state_t *state) {
  return readAddrFromAddr(state,readPCByte(state))+state->reg.Y;
}

/* Instructions that read from a value are passed the argument by value. This
 * allows an instruction function to operate with a register or with a memory
 * location which requires readByteFromAddr(). Instructions that write to a
 * location are provided with a pointer. If we OR with a special memory
 * location the flags will not be random like they should (what does it mean
 * to write to a hardware mapped memory location?)
 *
 * Instructions taken from www.obelix.demon.co.uk/6502/reference.html
 */

void branch(program_state_t *state, bool flag) {
  uint8_t val = readPCByte(state);
  if(flag) {
    state->program_counter += (int16_t)(int8_t)val;
  }
}

void pushStack(program_state_t *state, uint8_t byte) {
  state->memory[0x100+state->reg.S] = byte;
  state->reg.S -= 1;
}
 
uint8_t pullStack(program_state_t *state) {
  state->reg.S += 1;
  return state->memory[0x100+state->reg.S];
}

void ADC(program_state_t *state, uint8_t input) {
  int temp = (int)state->reg.A + (int)input + (state->flag.carry?1:0);
  state->reg.A = (uint8_t)temp;
  state->flag.carry = temp>0xff;
}

void AND(program_state_t *state, uint8_t input) {
  state->reg.A &= input;
  state->flag.zero     = (state->reg.A == 0);
  state->flag.negative = (state->reg.A & BIT_MASK(7));
}

void ASL(program_state_t *state, uint8_t *ptr) {
  state->flag.carry = (*ptr & BIT_MASK(7));
  state->flag.zero  = (*ptr == 0);
  *ptr <<= 1;
  state->flag.negative = (bool)(*ptr & BIT_MASK(7));
}

void BIT(program_state_t *state, uint8_t input) {
  uint8_t temp = input & state->reg.A;
  state->flag.zero     = (temp == 0);
  state->flag.overflow = (bool)(input & BIT_MASK(6));
  state->flag.negative = (bool)(input & BIT_MASK(7));
}

void CMP(program_state_t *state, uint8_t reg, uint8_t input) {
  /* also CPX and CPY */
  uint8_t temp = reg - input;
  state->flag.carry     = (reg >= input);
  state->flag.zero      = (reg == input);
  state->flag.negative  = (bool)(temp & BIT_MASK(7));
}

void DEC(program_state_t *state, uint8_t *ptr) {
  *ptr -= 1;
  state->flag.zero      = (*ptr == 0);
  state->flag.negative  = (bool)(*ptr & BIT_MASK(7));
}

void EOR(program_state_t *state, uint8_t input) {
  state->reg.A ^= input;
  state->flag.zero     = (state->reg.A == 0);
  state->flag.negative = (bool)(state->reg.A & BIT_MASK(7));
}

void INC(program_state_t *state, uint8_t *ptr) {
  *ptr += 1;
  state->flag.zero      = (*ptr == 0);
  state->flag.negative  = (bool)(*ptr & BIT_MASK(7));
}

void LD(program_state_t *state, uint8_t *reg, uint8_t input) {
  *reg = input;
  state->flag.zero      = (state->reg.A == 0);
  state->flag.negative  = (bool)(*reg & BIT_MASK(7));
}

void LSR(program_state_t *state, uint8_t *ptr) {
  state->flag.carry = (*ptr & BIT_MASK(0));
  *ptr >>= 1;
  state->flag.zero     = (*ptr==0);
  state->flag.negative = (bool)(*ptr & BIT_MASK(7));
}

void ORA(program_state_t *state, uint8_t input) {
  state->reg.A |= input;
  state->flag.zero     = (state->reg.A == 0);
  state->flag.negative = (bool)(state->reg.A & BIT_MASK(7));
}

void ROL(program_state_t *state, uint8_t *ptr) {
  uint8_t temp = *ptr;
  *ptr <<= 1;
  if(state->flag.carry) {
    *ptr |= BIT_MASK(0);
  } else {
    *ptr &= (0xff - BIT_MASK(0));
  }
  state->flag.carry    = (temp & BIT_MASK(7));
  state->flag.zero     = (bool)(temp == 0);
  state->flag.negative = (bool)(*ptr & BIT_MASK(7));
}

void ROR(program_state_t *state, uint8_t *ptr) {
  uint8_t temp = *ptr;
  *ptr >>= 1;
  if(state->flag.carry) {
    *ptr |= BIT_MASK(7);
  } else {
    *ptr &= (0xff - BIT_MASK(7));
  }
  state->flag.carry    = (temp & BIT_MASK(0));
  state->flag.zero     = (bool)(temp == 0);
  state->flag.negative = (bool)(*ptr & BIT_MASK(7));
}

void SBC(program_state_t *state, uint8_t input) {
  if(false && state->flag.decimal_mode) {
    /* TODO */
  } else {
    int temp = (int)state->reg.A - (int)input - (!state->flag.carry?1:0);
    state->reg.A = (uint8_t)(temp&0xff);
    state->flag.carry    = (temp>=0x00);
    state->flag.zero     = (state->reg.A == 0);
    state->flag.overflow = (temp < -128);
    state->flag.negative = (bool)(state->reg.A & BIT_MASK(7));
  }
}

int executeInstruction(program_state_t *state) {
  uint8_t val;
  uint16_t addr;
  val = readPCByte(state);

  switch(val) {
    case 0x00: /* BRK */
      fprintf(logfile,"Terminating on BRK instruction\n");
      return -1;
    case 0x01: /* ORA */
      addr = getIndirectXRef(state);
      ORA(state,readByteFromAddr(state,addr));
      break;
    case 0x05: /* ORA */
      addr = getZeroPageRef(state);
      ORA(state,readByteFromAddr(state,addr));
      break;
    case 0x06: /* ASL */
      addr = getZeroPageRef(state);
      ASL(state,state->memory+addr);
      break;
    case 0x08: /* PHP */
      pushStack(state,getStatusByte(state));
      break;
    case 0x09: /* ORA */
      ORA(state,readPCByte(state));
      break;
    case 0x0a: /* ASL */
      ASL(state,&state->reg.A);
      break;
    case 0x0d: /* ORA */
      addr = getAbsoluteRef(state);
      ORA(state,readPCByte(state));
      break;
    case 0x0e: /* ASL */
      addr = getAbsoluteRef(state);
      ASL(state,state->memory+addr);
      break;
    case 0x10: /* BPL */
      branch(state,!state->flag.negative);
      break;
    case 0x11: /* ORA */
      addr = getIndirectYRef(state);
      ORA(state,readByteFromAddr(state,addr));
      break;
    case 0x15: /* ORA */
      addr = getZeroPageXRef(state);
      ORA(state,readByteFromAddr(state,addr));
      break;
    case 0x16: /* ASL */
      addr = getZeroPageXRef(state);
      ASL(state,state->memory+addr);
      break;
    case 0x18: /* CLC */
      state->flag.carry = false;
      break;
    case 0x19: /* ORA */
      addr = getAbsYRef(state);
      ORA(state,readByteFromAddr(state,addr));
      break;
    case 0x1d: /* ORA */
      addr = getAbsXRef(state);
      ORA(state,readByteFromAddr(state,addr));
      break;
    case 0x1e: /* ASL */
      addr = getAbsXRef(state);
      ASL(state,state->memory+addr);
      break;
    case 0x20: /* JSR */
      addr = getAbsoluteRef(state);
      pushStack(state,low (--state->program_counter));
      pushStack(state,high(  state->program_counter));
      state->program_counter = addr;
      break;
    case 0x21: /* AND */
      addr = getIndirectXRef(state);
      AND(state,readByteFromAddr(state,addr));
      break;
    case 0x24: /* BIT */
      addr = getZeroPageRef(state);
      BIT(state,readByteFromAddr(state,addr));
      break;
    case 0x25: /* AND */
      addr = getZeroPageRef(state);
      AND(state,readByteFromAddr(state,addr));
      break;
    case 0x26: /* ROL */
      addr = getZeroPageRef(state);
      ROL(state,state->memory+addr);
      break;
    case 0x28: /* PLP */
      setStatusByte(state,pullStack(state));
      break;
    case 0x29: /* AND */
      AND(state,readPCByte(state));
      break;
    case 0x2a: /* ROL */
      ROL(state,&state->reg.A);
      break;
    case 0x2c: /* BIT */
      addr = getAbsoluteRef(state);
      BIT(state,readByteFromAddr(state,addr));
      break;
    case 0x2d: /* AND */
      addr = getAbsoluteRef(state);
      AND(state,readByteFromAddr(state,addr));
      break;
    case 0x2e: /* ROL */
      addr = getAbsoluteRef(state);
      ROL(state,state->memory+addr);
      break;
    case 0x30: /* BMI */
      branch(state,state->flag.negative);
      break;
    case 0x31: /* AND */
      addr = getIndirectYRef(state);
      AND(state,readByteFromAddr(state,addr));
      break;
    case 0x35: /* AND */
      addr = getZeroPageXRef(state);
      AND(state,readByteFromAddr(state,addr));
      break;
    case 0x36: /* ROL */
      addr = getZeroPageXRef(state);
      ROL(state,state->memory+addr);
      break;
    case 0x38: /* SEC */
      state->flag.carry = true;
      break;
    case 0x39: /* AND */
      addr = getAbsYRef(state);
      AND(state,readByteFromAddr(state,addr));
      break;
    case 0x3d: /* AND */
      addr = getAbsXRef(state);
      AND(state,readByteFromAddr(state,addr));
      break;
    case 0x3e: /* ROL */
      addr = getAbsXRef(state);
      ROL(state,state->memory+addr);
      break;
    case 0x40: /* RTI */
      setStatusByte(state,pullStack(state));
      val = pullStack(state);
      state->program_counter = combine(val,pullStack(state));
      break;
    case 0x41: /* EOR */
      addr = getIndirectXRef(state);
      EOR(state,readByteFromAddr(state,addr));
      break;
    case 0x45: /* EOR */
      addr = getZeroPageRef(state);
      EOR(state,readByteFromAddr(state,addr));
      break;
    case 0x46: /* LSR */
      addr = getZeroPageRef(state);
      LSR(state,state->memory+addr);
      break;
    case 0x48: /* PHA */
      pushStack(state,state->reg.A);
      break;
    case 0x49: /* EOR */
      EOR(state,readPCByte(state));
      break;
    case 0x4a: /* LSR */
      LSR(state,&state->reg.A);
      break;
    case 0x4c: /* JMP */
      addr = getAbsoluteRef(state);
      state->program_counter = addr;
      break;
    case 0x4d: /* EOR */
      addr = getAbsoluteRef(state);
      EOR(state,readByteFromAddr(state,addr));
      break;
    case 0x4e: /* LSR */
      addr = getAbsoluteRef(state);
      LSR(state,state->memory+addr);
      break;
    case 0x50: /* BVC */
      branch(state,!state->flag.overflow);
      break;
    case 0x51: /* EOR */
      addr = getIndirectYRef(state);
      EOR(state,readByteFromAddr(state,addr));
      break;
    case 0x55: /* EOR */
      addr = getZeroPageXRef(state);
      EOR(state,readByteFromAddr(state,addr));
      break;
    case 0x56: /* LSR */
      addr = getZeroPageXRef(state);
      LSR(state,state->memory+addr);
      break;
    case 0x58: /* CLI */
      state->flag.block_interrupts = false;
      break;
    case 0x59: /* EOR */
      addr = getAbsYRef(state);
      EOR(state,readByteFromAddr(state,addr));
      break;
    case 0x5d: /* EOR */
      addr = getAbsXRef(state);
      EOR(state,readByteFromAddr(state,addr));
      break;
    case 0x5e: /* LSR */
      addr = getAbsXRef(state);
      LSR(state,state->memory+addr);
      break;
    case 0x60: /* RTS */
      val = pullStack(state);
      state->program_counter = combine(pullStack(state),val) + 1;
      break;
    case 0x61: /* ADC */
      addr = getIndirectXRef(state);
      ADC(state,readByteFromAddr(state,addr));
      break;
    case 0x65: /* ADC */
      addr = getZeroPageRef(state);
      ADC(state,readByteFromAddr(state,addr));
      break;
    case 0x66: /* ROR */
      addr = getZeroPageRef(state);
      ROR(state,state->memory+addr);
      break;
    case 0x68: /* PLA */
      state->reg.A = pullStack(state);
      break;
    case 0x69: /* ADC */
      ADC(state,readPCByte(state));
      break;
    case 0x6a: /* ROR */
      ROR(state,&state->reg.A);
      break;
    case 0x6c: /* JMP */
      state->program_counter = readAddrFromAddr(state,getAbsoluteRef(state));
      break;
    case 0x6d: /* ADC */
      addr = getAbsoluteRef(state);
      ADC(state,readByteFromAddr(state,addr));
      break;
    case 0x6e: /* ROR */
      addr = getAbsoluteRef(state);
      ROR(state,state->memory+addr);
      break;
    case 0x70: /* BVS */
      branch(state,state->flag.overflow);
      break;
    case 0x71: /* ADC */
      addr = getIndirectYRef(state);
      ADC(state,readByteFromAddr(state,addr));
      break;
    case 0x75: /* ADC */
      addr = getZeroPageXRef(state);
      ADC(state,readByteFromAddr(state,addr));
      break;
    case 0x76: /* ROR */
      addr = getZeroPageXRef(state);
      ROR(state,state->memory+addr);
      break;
    case 0x78: /* SEI */
      state->flag.block_interrupts = true;
      break;
    case 0x79: /* ADC */
      addr = getAbsYRef(state);
      ADC(state,readByteFromAddr(state,addr));
      break;
    case 0x7d: /* ADC */
      addr = getAbsXRef(state);
      ADC(state,readByteFromAddr(state,addr));
      break;
    case 0x7e: /* ROR */
      addr = getAbsXRef(state);
      ROR(state,state->memory+addr);
      break;
    case 0x81: /* STA */
      addr = getIndirectXRef(state);
      state->memory[addr] = state->reg.A;
      break;
    case 0x84: /* STY */
      addr = getZeroPageRef(state);
      state->memory[addr] = state->reg.Y;
      break;
    case 0x85: /* STA */
      addr = getZeroPageRef(state);
      state->memory[addr] = state->reg.A;
      break;
    case 0x86: /* STX */
      addr = getZeroPageRef(state);
      state->memory[addr] = state->reg.X;
      break;
    case 0x88: /* DEY */
      DEC(state,&state->reg.Y);
      break;
    case 0x8a: /* TXA */
      state->reg.A = state->reg.X;
      state->flag.negative = (bool)(state->reg.A & BIT_MASK(7));
      state->flag.zero     = (bool)(state->reg.A == 0);
      break;
    case 0x8c: /* STY */
      addr = getAbsoluteRef(state);
      state->memory[addr] = state->reg.Y;
      break;
    case 0x8d: /* STA */
      addr = getAbsoluteRef(state);
      state->memory[addr] = state->reg.A;
      break;
    case 0x8e: /* STX */
      addr = getAbsoluteRef(state);
      state->memory[addr] = state->reg.X;
      break;
    case 0x90: /* BCC */
      branch(state,!state->flag.carry);
      break;
    case 0x91: /* STA */
      addr = getIndirectYRef(state);
      state->memory[addr] = state->reg.A;
      break;
    case 0x94: /* STY */
      addr = getZeroPageXRef(state);
      state->memory[addr] = state->reg.Y;
      break;
    case 0x95: /* STA */
      addr = getZeroPageXRef(state);
      state->memory[addr] = state->reg.A;
      break;
    case 0x96: /* STX */
      addr = getZeroPageYRef(state);
      state->memory[addr] = state->reg.X;
      break;
    case 0x98: /* TYA */
      state->reg.A = state->reg.Y;
      state->flag.negative = (bool)(state->reg.A & BIT_MASK(7));
      state->flag.zero     = (bool)(state->reg.A == 0);
      break;
    case 0x99: /* STA */
      addr = getAbsYRef(state);
      state->memory[addr] = state->reg.A;
      break;
    case 0x9a: /* TXS */
      state->reg.S = state->reg.X;
      break;
    case 0x9d: /* STA */
      addr = getAbsXRef(state);
      state->memory[addr] = state->reg.A;
      break;
    case 0xa0: /* LDY */
      LD(state,&state->reg.Y,readPCByte(state));
      break;
    case 0xa1: /* LDA */
      addr = getIndirectXRef(state);
      LD(state,&state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xa2: /* LDX */
      LD(state,&state->reg.X,readPCByte(state));
      break;
    case 0xa4: /* LDY */
      addr = getZeroPageRef(state);
      LD(state,&state->reg.Y,readByteFromAddr(state,addr));
      break;
    case 0xa5: /* LDA */
      addr = getZeroPageRef(state);
      LD(state,&state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xa6: /* LDX */
      addr = getZeroPageRef(state);
      LD(state,&state->reg.X,readByteFromAddr(state,addr));
      break;
    case 0xa8: /* TAY */
      state->reg.Y = state->reg.A;
      state->flag.negative = (bool)(state->reg.Y & BIT_MASK(7));
      state->flag.zero     = (bool)(state->reg.Y == 0);
      break;
    case 0xa9: /* LDA */
      LD(state,&state->reg.A,readPCByte(state));
      break;
    case 0xaa: /* TAX */
      state->reg.X = state->reg.A;
      state->flag.negative = (bool)(state->reg.X & BIT_MASK(7));
      state->flag.zero     = (bool)(state->reg.X == 0);
      break;
    case 0xac: /* LDY */
      addr = getAbsoluteRef(state);
      LD(state,&state->reg.Y,readByteFromAddr(state,addr));
      break;
    case 0xad: /* LDA */
      addr = getAbsoluteRef(state);
      LD(state,&state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xae: /* LDX */
      addr = getAbsoluteRef(state);
      LD(state,&state->reg.X,readByteFromAddr(state,addr));
      break;
    case 0xb0: /* BCS */
      branch(state,state->flag.carry);
      break;
    case 0xb1: /* LDA */
      addr = getIndirectYRef(state);
      LD(state,&state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xb4: /* LDY */
      addr = getZeroPageXRef(state);
      LD(state,&state->reg.Y,readByteFromAddr(state,addr));
      break;
    case 0xb5: /* LDA */
      addr = getZeroPageXRef(state);
      LD(state,&state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xb6: /* LDX */
      addr = getZeroPageYRef(state);
      LD(state,&state->reg.X,readByteFromAddr(state,addr));
      break;
    case 0xb8: /* CLV */
      state->flag.overflow = false;
      break;
    case 0xb9: /* LDA */
      addr = getAbsYRef(state);
      LD(state,&state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xba: /* TSX */
      state->reg.X = state->reg.S;
      break;
    case 0xbc: /* LDY */
      addr = getAbsXRef(state);
      LD(state,&state->reg.Y,readByteFromAddr(state,addr));
      break;
    case 0xbd: /* LDA */
      addr = getAbsXRef(state);
      LD(state,&state->reg.A,readByteFromAddr(state,addr));
      LD(state,&state->reg.A,readByteFromAddr(state,addr));
      break;
      break;
    case 0xbe: /* LDX */
      addr = getAbsYRef(state);
      LD(state,&state->reg.X,readByteFromAddr(state,addr));
      break;
    case 0xc0: /* CPY */
      CMP(state,state->reg.Y,readPCByte(state));
      break;
    case 0xc1: /* CMP */
      addr = getIndirectXRef(state);
      CMP(state,state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xc4: /* CPY */
      addr = getZeroPageRef(state);
      CMP(state,state->reg.Y,readByteFromAddr(state,addr));
      break;
    case 0xc5: /* CMP */
      addr = getZeroPageRef(state);
      CMP(state,state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xc6: /* DEC */
      addr = getZeroPageRef(state);
      DEC(state,state->memory+addr);
      break;
    case 0xc8: /* INY */
      INC(state,&state->reg.Y);
      break;
    case 0xc9: /* CMP */
      CMP(state,state->reg.A,readPCByte(state));
      break;
    case 0xca: /* DEX */
      DEC(state,&state->reg.X);
      break;
    case 0xcc: /* CPY */
      addr = getAbsoluteRef(state);
      CMP(state,state->reg.Y,readByteFromAddr(state,addr));
      break;
    case 0xcd: /* CMP */
      addr = getAbsoluteRef(state);
      CMP(state,state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xce: /* DEC */
      addr = getAbsoluteRef(state);
      DEC(state,state->memory+addr);
      break;
    case 0xd0: /* BNE */
      branch(state,!state->flag.zero);
      break;
    case 0xd1: /* CMP */
      addr = getIndirectYRef(state);
      CMP(state,state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xd5: /* CMP */
      addr = getZeroPageXRef(state);
      CMP(state,state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xd6: /* DEC */
      addr = getZeroPageXRef(state);
      DEC(state,state->memory+addr);
      break;
    case 0xd8: /* CLD */
      state->flag.decimal_mode = false;
      break;
    case 0xd9: /* CMP */
      addr = getAbsYRef(state);
      CMP(state,state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xdd: /* CMP */
      addr = getAbsXRef(state);
      CMP(state,state->reg.A,readByteFromAddr(state,addr));
      break;
    case 0xde: /* DEC */
      addr = getAbsXRef(state);
      DEC(state,state->memory+addr);
      break;
    case 0xe0: /* CPX */
      CMP(state,state->reg.X,readPCByte(state));
      break;
    case 0xe1: /* SBC */
      addr = getIndirectXRef(state);
      SBC(state,readByteFromAddr(state,addr));
      break;
    case 0xe4: /* CPX */
      addr = getZeroPageRef(state);
      CMP(state,state->reg.X,readByteFromAddr(state,addr));
      break;
    case 0xe5: /* SBC */
      addr = getZeroPageRef(state);
      SBC(state,readByteFromAddr(state,addr));
      break;
    case 0xe6: /* INC */
      addr = getZeroPageRef(state);
      INC(state,state->memory+addr);
      break;
    case 0xe8: /* INX */
      INC(state,&state->reg.X);
      break;
    case 0xe9: /* SBC */
      SBC(state,readPCByte(state));
      break;
    case 0xea: /* NOP */
      break;
    case 0xec: /* CPX */
      addr = getAbsoluteRef(state);
      CMP(state,state->reg.X,readByteFromAddr(state,addr));
      break;
    case 0xed: /* SBC */
      addr = getAbsoluteRef(state);
      SBC(state,readByteFromAddr(state,addr));
      break;
    case 0xee: /* INC */
      addr = getAbsoluteRef(state);
      INC(state,state->memory+addr);
      break;
    case 0xf0: /* BEQ */
      branch(state,state->flag.zero);
      break;
    case 0xf1: /* SBC */
      addr = getIndirectYRef(state);
      SBC(state,readByteFromAddr(state,addr));
      break;
    case 0xf5: /* SBC */
      addr = getZeroPageXRef(state);
      SBC(state,readByteFromAddr(state,addr));
      break;
    case 0xf6: /* INC */
      addr = getZeroPageXRef(state);
      INC(state,state->memory+addr);
      break;
    case 0xf8: /* SED */
      state->flag.decimal_mode = true;
      break;
    case 0xf9: /* SBC */
      addr = getAbsYRef(state);
      SBC(state,readByteFromAddr(state,addr));
      break;
    case 0xfd: /* SBC */
      addr = getAbsXRef(state);
      SBC(state,readByteFromAddr(state,addr));
      break;
    case 0xfe: /* INC */
      addr = getAbsXRef(state);
      INC(state,state->memory+addr);
      break;
    default:
      fprintf(logfile,"Bad opcode (%02x) at %04x\n",val,state->program_counter);
      return -1;
  }

  return 0;
}


/* Entry */

int verbRun(const char *argv[]) {
  logfile = fopen("log", "a");
  if(!logfile) {
    int err = errno;
    const char *error_msg = strerror(err);
    printf("Error opening log file file \"log\"\n");
    printf(" %s\n", error_msg);
    return err;
  }

  program_state_t state;
  if(initProgramState(&state))    return -1;

  if(loadProgram(argv[2],&state)) {
    cleanUpProgramState(&state);
    return -1;
  }

  while(!executeInstruction(state)) { };
  
  fclose(logfile);
  return 0;
}




